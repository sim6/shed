# Documentation

There are several kind of documents in this directory.

Reference for the _Shed_ commands:

* [`reference-hub.md`](reference-hub.md): the online documentation for all [`shed-hub`](../shed-hub) commands collected in a Markdown document.

* [`reference-image.md`](reference-image.md): the online documentation for all [`shed-image`](../shed-image) commands collected in a Markdown document.

* [`reference-container.md`](reference-container.md): the online documentation for all [`shed-container`](../shed-container) commands collected in a Markdown document.

Slides:

* [`slides-es.html`](slides-es.html): we intend to build a  complete slide presentation for _Shed_. _Work i progress_&hellip;

<!--
vim:syntax=markdown:et:ts=4:sw=4:ai
-->
