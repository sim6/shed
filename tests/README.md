# Tests

Example tests grouped in this directory:

* **ECHO**: this example implements the ECHO protocol server using two linked containers.

* **propagate**: this example show how to create multiple linked containers.

* **supervised**: this example show how to run a Bash script inside the container with PID 1 and
a supervised worker service as a background job.

<!--
vim:syntax=markdown:et:ts=4:sw=4:ai
-->
